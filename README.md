# OpenTerm-Mac

![OpenTerm Example](/Resources/Open-Term-Mac.gif)


## About
This is small Mac application that allows a user to open a terminal with a present working directory (pwd) correposnding to the path of a Finder window.  The program is a relatively simple AppleScript that has been compiled into Mac application that can be placed in the Finder menu bar.


## Downloads

You can download a pre-compiled binary of the Mac app via the tags page on the repo. 


*  [OpenTerm v1.0](https://gitlab.com/kpmoran-public/OpenTerm-Mac/tags)

